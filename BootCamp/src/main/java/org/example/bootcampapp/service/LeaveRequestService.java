package org.example.bootcampapp.service;


import lombok.RequiredArgsConstructor;
import org.example.bootcampapp.model.entity.LeaveRequest;
import org.example.bootcampapp.model.repository.LeaveRequestRepository;
import org.example.bootcampapp.enums.LeaveRequestStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LeaveRequestService {

    private final LeaveRequestRepository leaveRequestRepository;

    // Create operation
    public LeaveRequest createLeaveRequest(LeaveRequest leaveRequest) {
        return leaveRequestRepository.save(leaveRequest);
    }

    // Read operation
    public List<LeaveRequest> getLeaveRequestsByEmployeeId(Long employeeId) {
        return leaveRequestRepository.findByEmployeeId(employeeId);
    }

    public Optional<LeaveRequest> getLeaveRequestById(Long id) {
        return leaveRequestRepository.findById(id);
    }

    // Update operation
    public LeaveRequest acceptLeaveRequest(Long requestId) {
        Optional<LeaveRequest> optionalLeaveRequest = leaveRequestRepository.findById(requestId);
        if (optionalLeaveRequest.isPresent()) {
            LeaveRequest leaveRequest = optionalLeaveRequest.get();
            leaveRequest.setStatus(LeaveRequestStatus.ACCEPTED);
            return leaveRequestRepository.save(leaveRequest);
        } else {
            throw new RuntimeException("Leave request not found with id: " + requestId);
        }
    }

    public LeaveRequest declineLeaveRequest(Long requestId, String reason) {
        Optional<LeaveRequest> optionalLeaveRequest = leaveRequestRepository.findById(requestId);
        if (optionalLeaveRequest.isPresent()) {
            LeaveRequest leaveRequest = optionalLeaveRequest.get();
            leaveRequest.setStatus(LeaveRequestStatus.DECLINED);
            leaveRequest.setDeclineReason(reason);
            return leaveRequestRepository.save(leaveRequest);
        } else {
            throw new RuntimeException("Leave request not found with id: " + requestId);
        }
    }

    // Delete operation
    public void deleteLeaveRequest(Long id) {
        leaveRequestRepository.deleteById(id);
    }
}
