package org.example.bootcampapp.service;


import lombok.RequiredArgsConstructor;
import org.example.bootcampapp.model.dto.AdminDTO;
import org.example.bootcampapp.model.entity.Admin;
import org.example.bootcampapp.model.mappers.AdminMapper;
import org.example.bootcampapp.model.repository.AdminRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdminService {

    private final AdminRepository adminRepository;
    private final AdminMapper adminMapper;



    public AdminDTO createAdmin(AdminDTO adminDTO) {
        Admin admin = adminMapper.toEntity(adminDTO);
        Admin savedAdmin = adminRepository.save(admin);
        return adminMapper.toDto(savedAdmin);
    }

    public List<AdminDTO> getAllAdmins() {
        List<Admin> admins = adminRepository.findAll();
        return admins.stream()
                .map(adminMapper::toDto)
                .collect(Collectors.toList());
    }

    public Optional<AdminDTO> getAdminById(Long id) {
        Optional<Admin> adminOptional = adminRepository.findById(id);
        return adminOptional.map(adminMapper::toDto);
    }
    public AdminDTO updateAdmin(Long id, AdminDTO adminDTO) {
        Optional<Admin> optionalAdmin = adminRepository.findById(id);
        if (optionalAdmin.isPresent()) {
            Admin admin = adminMapper.toEntity(adminDTO);
            admin.setId(id); // Ensure the ID is set
            Admin updatedAdmin = adminRepository.save(admin);
            return adminMapper.toDto(updatedAdmin);
        } else {
            throw new RuntimeException("Admin not found with id: " + id);
        }
    }

    // Delete operation
    public void deleteAdmin(Long id) {
        adminRepository.deleteById(id);
    }
}