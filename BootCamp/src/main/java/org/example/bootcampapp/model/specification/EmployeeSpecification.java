package org.example.bootcampapp.model.specification;

import io.micrometer.common.util.StringUtils;
import jakarta.persistence.criteria.Predicate;
import org.example.bootcampapp.model.entity.Employee;
import org.springframework.data.jpa.domain.Specification;

public class EmployeeSpecification {

    public static Specification<Employee> filterEmployee(String firstName, String lastName, String department, String position) {
        return (root, query, criteriaBuilder) -> {
            Predicate firstNamePredicate =
                    criteriaBuilder.like(root.get("firstName"), StringUtils.isBlank(firstName)
                            ? likePattern("") : firstName);
            Predicate lastNamePredicate =
                    criteriaBuilder.like(root.get("lastName"), StringUtils.isBlank(lastName)
                            ? likePattern("") : lastName);
            Predicate departmentPredicate =
                    criteriaBuilder.like(root.get("department"), StringUtils.isBlank(department)
                            ? likePattern("") : department);
            Predicate positionPredicate =
                    criteriaBuilder.like(root.get("position"), StringUtils.isBlank(position)
                            ? likePattern("") : position);
            return criteriaBuilder.and(firstNamePredicate, lastNamePredicate, departmentPredicate, positionPredicate);
        };
    }

    private static String likePattern(String value) {
        return "%" + value + "%";
    }
}