package org.example.bootcampapp.model.entity;


import jakarta.persistence.Entity;
import lombok.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Employee extends BaseEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    protected Long id;
    private String firstName;
    private String lastName;
    private String department;
    private String position;


}
