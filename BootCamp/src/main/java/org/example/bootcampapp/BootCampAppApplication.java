package org.example.bootcampapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootCampAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootCampAppApplication.class, args);
	}

}
