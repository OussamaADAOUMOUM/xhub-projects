package org.example.bootcampapp.model.dto;

import lombok.Data;

@Data
public class AdminDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}