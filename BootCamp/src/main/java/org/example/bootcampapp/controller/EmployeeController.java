package org.example.bootcampapp.controller;

import lombok.RequiredArgsConstructor;
import org.example.bootcampapp.model.entity.Employee;
import org.example.bootcampapp.model.entity.LeaveRequest;
import org.example.bootcampapp.service.EmployeeService;
import org.example.bootcampapp.service.LeaveRequestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;
    private final LeaveRequestService leaveRequestService;

    // Access and update profile information
    @GetMapping("/profile/{id}")
    @PreAuthorize("hasRole('Employee')")
    public ResponseEntity<Employee> getEmployeeProfile(@PathVariable Long id) {
        Optional<Employee> employee = employeeService.getEmployeeById(id);
        return employee.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/profile/{id}")
    @PreAuthorize("hasRole('Employee')")
    public ResponseEntity<Employee> updateEmployeeProfile(@PathVariable Long id, @RequestBody Employee employeeDetails) {
        Employee updatedEmployee = employeeService.updateEmployee(id, employeeDetails);
        return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
    }

    // Send a leave request
    @PostMapping("/leave-requests")
    @PreAuthorize("hasRole('Employee')")
    public ResponseEntity<LeaveRequest> sendLeaveRequest(@RequestBody LeaveRequest leaveRequest) {
        LeaveRequest createdLeaveRequest = leaveRequestService.createLeaveRequest(leaveRequest);
        return new ResponseEntity<>(createdLeaveRequest, HttpStatus.CREATED);
    }

    // View the status of the request
    @GetMapping("/leave-requests/{requestId}")
    @PreAuthorize("hasRole('Employee')")
    public ResponseEntity<LeaveRequest> viewLeaveRequestStatus(@PathVariable Long requestId) {
        Optional<LeaveRequest> leaveRequest = leaveRequestService.getLeaveRequestById(requestId);
        return leaveRequest.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}