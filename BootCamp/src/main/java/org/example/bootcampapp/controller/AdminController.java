package org.example.bootcampapp.controller;


import lombok.RequiredArgsConstructor;
import org.example.bootcampapp.model.entity.Employee;
import org.example.bootcampapp.model.entity.LeaveRequest;
import org.example.bootcampapp.model.mappers.AdminMapper;
import org.example.bootcampapp.service.AdminService;
import org.example.bootcampapp.service.EmployeeService;
import org.example.bootcampapp.service.LeaveRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {

    private final AdminService adminService;
    private final AdminMapper adminMapper;
    private final EmployeeService employeeService;
    private final LeaveRequestService leaveRequestService;

    // Create operation
    @PostMapping("/employees")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        Employee createdEmployee = employeeService.createEmployee(employee);
        return new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);
    }

    // Read operation
    @GetMapping("/employees")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<Employee>> getAllEmployees(
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String lastName,
            @RequestParam(required = false) String department,
            @RequestParam(required = false) String position

    ) {
        return ResponseEntity.ok(employeeService.getAllEmployees(firstName, lastName, department, position));
    }

    @GetMapping("/employees/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
        Optional<Employee> employee = employeeService.getEmployeeById(id);
        return employee.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // Update operation
    @PutMapping("/employees/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id, @RequestBody Employee employeeDetails) {
        Employee updatedEmployee = employeeService.updateEmployee(id, employeeDetails);
        return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
    }

    // Delete operation
    @DeleteMapping("/employees/{id}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<Void> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // View leave request
    @GetMapping("/leave-requests/{employeeId}")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<List<LeaveRequest>> viewLeaveRequests(@PathVariable Long employeeId) {
        List<LeaveRequest> leaveRequests = leaveRequestService.getLeaveRequestsByEmployeeId(employeeId);
        return new ResponseEntity<>(leaveRequests, HttpStatus.OK);
    }

    // Accept leave request
    @PostMapping("/leave-requests/{requestId}/accept")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<LeaveRequest> acceptLeaveRequest(@PathVariable Long requestId) {
        LeaveRequest acceptedRequest = leaveRequestService.acceptLeaveRequest(requestId);
        return new ResponseEntity<>(acceptedRequest, HttpStatus.OK);
    }

    // Decline leave request
    @PostMapping("/leave-requests/{requestId}/decline")
    public ResponseEntity<LeaveRequest> declineLeaveRequest(@PathVariable Long requestId, @RequestBody String reason) {
        LeaveRequest declinedRequest = leaveRequestService.declineLeaveRequest(requestId, reason);
        return new ResponseEntity<>(declinedRequest, HttpStatus.OK);
    }
}