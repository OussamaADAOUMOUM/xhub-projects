package org.example.bootcampapp.service;


import lombok.RequiredArgsConstructor;
import org.example.bootcampapp.model.entity.Employee;
import org.example.bootcampapp.model.repository.EmployeeRepository;
import org.springframework.stereotype.Service;
import org.example.bootcampapp.model.specification.EmployeeSpecification;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    // Create operation
    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    // Read operation
    public List<Employee> getAllEmployees(String firstName, String lastName, String department, String position) {
        final Specification<Employee> specification =
                EmployeeSpecification.filterEmployee(firstName, lastName, department, position);
        final List<Employee> employees = employeeRepository.findAll(specification);
        return employees;
    }

    public Optional<Employee> getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    // Update operation
    public Employee updateEmployee(Long id, Employee employeeDetails) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();
            employee.setFirstName(employeeDetails.getFirstName());
            employee.setLastName(employeeDetails.getLastName());
            employee.setDepartment(employeeDetails.getDepartment());
            employee.setPosition(employeeDetails.getPosition());
            return employeeRepository.save(employee);
        } else {
            throw new RuntimeException("Employee not found with id: " + id);
        }
    }

    // Delete operation
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }
}