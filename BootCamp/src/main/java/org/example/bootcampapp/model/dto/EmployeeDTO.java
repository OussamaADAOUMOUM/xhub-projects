package org.example.bootcampapp.model.dto;

public class EmployeeDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String department;
    private String position;
}
