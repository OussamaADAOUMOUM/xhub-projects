package org.example.bootcampapp.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class BootCampController {
    @GetMapping("/getAdminDetails")
    @PreAuthorize("hasRole('Admin')")
    public ResponseEntity<String> gatAdminDetails() {
        return ResponseEntity.ok( "Hello Admin from Spring boot & Keycloak - ADMIN");
    }

    @GetMapping("/getEmployeeDetails")
    @PreAuthorize("hasRole('Employee')")
    public ResponseEntity<String> gatEmployeeDetails() {
        return ResponseEntity.ok( "Hello Employee from Spring boot & Keycloak - ADMIN");
    }
}
