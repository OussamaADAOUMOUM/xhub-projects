package org.example.bootcampapp.model.mappers;

import org.example.bootcampapp.model.dto.AdminDTO;
import org.example.bootcampapp.model.entity.Admin;
import org.example.bootcampapp.model.mappers.AdminMapper;
import org.springframework.stereotype.Component;

@Component
public class AdminMapperImpl implements AdminMapper {

    @Override
    public AdminDTO toDto(Admin admin) {
        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setId(admin.getId());
        adminDTO.setFirstName(admin.getFirstName());
        adminDTO.setLastName(admin.getLastName());
        adminDTO.setEmail(admin.getEmail());
        // Map other properties if needed
        return adminDTO;
    }

    @Override
    public Admin toEntity(AdminDTO adminDTO) {
        Admin admin = new Admin();
        admin.setId(adminDTO.getId());
        admin.setFirstName(adminDTO.getFirstName());
        admin.setLastName(adminDTO.getLastName());
        admin.setEmail(adminDTO.getEmail());
        // Map other properties if needed
        return admin;
    }
}
