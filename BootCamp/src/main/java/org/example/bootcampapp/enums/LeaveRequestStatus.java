package org.example.bootcampapp.enums;

public enum LeaveRequestStatus {
    PENDING,
    ACCEPTED,
    DECLINED
}