package org.example.bootcampapp.model.mappers;

import org.example.bootcampapp.model.dto.AdminDTO;
import org.example.bootcampapp.model.entity.Admin;

public interface AdminMapper {
    AdminDTO toDto(Admin admin);
    Admin toEntity(AdminDTO adminDTO);
}